$(document).ready(function() {
    'use strict';
    // Audio Player
    $('audio').audioPlayer();
    // Pre Loader
    $(window).on("load", function(){
      $('#preloader').fadeOut(1000); // set duration in brackets    
    });
    // WOW Animation js
    // WOW - v1.0.2 - 2014-10-28
    // Copyright (c) 2014 Matthieu Aussaguel; Licensed MIT
    new WOW({ mobile: false }).init();
    // Scroll to    
    $('.lovely-ma-scroll').click(function(e) {
        e.preventDefault();
        $('html,body').stop(true).animate({scrollTop:$(this.hash).offset().top-55}, 500);
    });
});
// Google Maps
function initialize(){
    //define map
    var map;
    //lat lng
    myLatlng = new google.maps.LatLng(37.769725, -122.462154);
    //define style
    var styles = [
        {
            //set all color
            featureType: "all",
            stylers: [{ hue: "#e84393" }]
        },
        {
            //hide business
            featureType: "poi.business",
            elementType: "labels",
            stylers: [{ visibility: "off" }]
        }
    ];
    //map options
    var mapOptions = {
        zoom: 16,
        center: myLatlng ,
        mapTypeControlOptions: {mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']} ,
        panControl: false , //hide panControl
        zoomControl: true , //hide zoomControl
        mapTypeControl: false , //hide mapTypeControl
        scaleControl: false , //hide scaleControl
        streetViewControl: false , //hide streetViewControl
        overviewMapControl: false , //hide overviewMapControl
    }
    //adding attribute value
    map = new google.maps.Map(document.getElementById('tooplate_contact_map'), mapOptions);
    var styledMap = new google.maps.StyledMapType(styles,{name: "Styled Map"});
    map.mapTypes.set('map_style', styledMap);
    map.setMapTypeId('map_style');
    //add marker
    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title: 'Welcome to Yangon'
    });
}
google.maps.event.addDomListener(window, 'load', initialize);
google.maps.event.addDomListener(window, 'resize', initialize);